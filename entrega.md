# Practica Final
## Parte bá sica
En la parte basica de mi trabajo, se ha realizado todo y funciona correctamente, excepto el "bye" debido a que no he sabido arreglar un error que he arrastrado hasta el último día.....
(el error era el siguiente --> [WinError 10038] Se intentó realizar una operación en un elemento que no es un socket) 

En cuanto al error, pienso que es un error de indentación, pero no he sabido resolverlo..

## Parte adicional
* Gestión de errores

Se añaden los errores, el programa manda el error si la petición SIP no esta bien realizada.
* Cabecera de tamaño

Se incluye en los paquetes SIP la cabecera de tamaño del cuerpo del paquete, podemos encontrarlo en el client.py.
![](tama%C3%B1o.JPG)
* Envío por cliente

Cliente envía en cada sesión que establezca con ServidorRTP un mensaje de audio, así el ServidorRTP almacenará todos los ficheros de audio que reciba de los clientes
* Tiempo de expiración

Realizado.

Las partes adicionales de peticiones concurrentes y configuraciones adicionales, no he sabido realizarlas.