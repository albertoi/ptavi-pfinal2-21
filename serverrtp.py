#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""


import socketserver
import sys
import simplertp
import time
import socket

SERVER = "localhost"
ip = str(sys.argv[1].split(":")[0])
port = int(sys.argv[1].split(":")[1])
audio_file = str(sys.argv[2])
EXPIRES = str(3600)
cliente = audio_file.split(".")[0] + "@signasong.net"


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """

    def handle(self):
        global sender
        data = self.rfile.read().decode("utf-8")
        v = "v = 0"
        o = "o = "
        t = "t = 0"
        m = "m = audio 3543 + RTP"
        body = "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m
        print(data.split()[0])
        if data.split()[0] == "INVITE":
            TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
            print(TIME + " " + "SIP" + " " + "from" + " " + ip + ":" + str(port) + ":" + " " + str(data))
            ok = "SIP/2.0 200 OK" + "\r\n"
            print(TIME + " " + "SIP" + " " + "to" + " " + ip + ":" + str(port) + ": " + "SIP/2.0 200 OK")
            self.wfile.write(bytes(ok, "utf-8"))
            self.wfile.write(ok.encode("utf-8") + body.encode("utf-8"))
        elif data.split()[0] == "ACK":
            portc = int(data.split("\r\n")[1].split(" ")[0])
            ipc = str(data.split("\r\n")[1].split(" ")[1])
            print("llego hasta aqui")
            TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
            print(TIME + " " + "SIP" + " " + "from" + " " + ip + ":" + str(port) + ":" + " " + str(data))

            sender = simplertp.RTPSender(ip=ipc, port=portc, file='cancion.mp3', printout=True)
            sender.send_threaded()
            time.sleep(10)
        elif data.split()[0] == "BYE":
            sender.finish()
            TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
            print(TIME + " " + "SIP" + " " + "from" + " " + ip + ":" + str(port) + ":" + " " + str(data))
            print(TIME + " " + "SIP" + " " + "to" + " " + ip + ":" + str(port) + ": " + "SIP/2.0 200 OK")
            print("Envío finalizado")
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n")
        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed")



def send(address):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, port))
            REQUEST = "REGISTER " + cliente + \
            " " + "SIP/2.0" + " sip:" + " \r\nExpires: " + EXPIRES + \
            " address " + address

            my_socket.sendto(bytes(REQUEST, 'utf-8'), (SERVER, port))

    except ConnectionRefusedError:
        print("Error conectando a servidor")


def main():

    try:
        serv = socketserver.UDPServer((SERVER, 0), EchoHandler)

        Port = serv.server_address[1]
        Ip = serv.server_address[0]
        address = str(Port) + ":" + Ip + "\r\n"
        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        print(TIME + " " + " Starting..." + " ")
        print(TIME + "server listening in" + " " + Ip + ":" + str(port))
        send(address)
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Fin de Conexión")
        sys.exit(0)


if __name__ == "__main__":
    main()
