#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
from recv_rtp import RTPHandler
import socketserver
import threading
import time

# Cliente UDP simple.

# Dirección IP y puerto del servidor.

try:
    SERVER = 'localhost'

    IPReg = str(sys.argv[1].split(":")[0])
    portReg = int(sys.argv[1].split(":")[1])
    IPProxy = str(sys.argv[2].split(":")[0])
    puertoProxy = int(sys.argv[2].split(":")[1])
    fichero = sys.argv[6]
    tiempo = sys.argv[5]
    addrClient = sys.argv[3].split(":")[1]
    addrServerRTP = sys.argv[4].split(":")[1]
except IndexError:
    sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, portReg))
        port = my_socket.getsockname()[1]  # mi puerto del socket
        IP = my_socket.getsockname()[0]  # mi ip del socket
        SOLICITUD = "REGISTER " + addrClient + \
                    " " + "SIP/2.0" + " sip:" + "\r\nExpires: " + tiempo + \
                    " address " + IP + ":" + str(port)
        my_socket.sendto(bytes(SOLICITUD, "utf-8"), (SERVER, portReg))
        data = my_socket.recv(2048)
        Answer = data.decode("utf-8")
        answer = Answer.split("\r\n")[0]
        if answer == "SIP/2.0 200 OK":
            my_socket.close()  # finalizar conexión con el register
            c = "Content-Type: application/sdp"
            c1 = "Content-Length:"
            v = "v = 0"
            o = "o = " + addrClient
            t = "t = 0"
            m = "m = audio 3543 + RTP"
            print("fin de la conexión")
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.connect((SERVER, puertoProxy))
                TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                print(TIME + " " + "SIP" + " " + "to" + " " + SERVER + ":" + str(puertoProxy) + ": " + "SIP/2.0 200 OK")
                body0 = v + o + "s = mi sesión" + t + m
                body = c + "\r\n" + c1 + " " + str(len(body0)) + "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesión" + "\r\n" + t + "\r\n" + m
                SOLICITUD = "INVITE" + " " + addrServerRTP + " SIP/2.0" + '\r\n' + body
                my_socket.sendto(bytes(SOLICITUD, "utf-8"), (SERVER, puertoProxy))
                print(TIME + " " + "SIP" + " " + "to " + SERVER + ":" + str(puertoProxy))
                data1 = my_socket.recv(2048)
                Answer2 = data1.decode("utf-8")
                answer2 = Answer2.split("\r\n")[0]
                if answer2 == "SIP/2.0 200 OK":
                    print(TIME + " " + "SIP" + " " + "from" + " " + SERVER + ":" + str(portReg))
                    RTPHandler.open_output(fichero)
                    try:
                        with socketserver.UDPServer(("localhost", 0), RTPHandler) as serv:
                            puerto = serv.server_address[1]
                            dIp = serv.server_address[0]
                            address1 = str(puerto) + " " + dIp
                            SOLICITUD = "ACK" + " sip:" + addrServerRTP + " SIP/2.0" + "\r\n" + address1
                            my_socket.sendto(bytes(SOLICITUD,'utf-8'), (SERVER, puertoProxy))
                            TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                            print(TIME + " " + "SIP" + " " + "to" + " " + SERVER + ":" + str(puertoProxy) + ": " + "ACK sip:" + addrClient + " SIP/2.0.")
                            threading.Thread(target=serv.serve_forever).start()
                            time.sleep(10)
                            # SOLICITUD = "BYE" + " " + addrServerRTP + " SIP/2.0" + '\r\n'
                            # my_socket.sendto(bytes(SOLICITUD, 'utf-8'), (SERVER, puertoProxy))
                            # TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                            # print(TIME + " " + "SIP" + " " + "to" + " " + SERVER + ":" + str(puertoProxy) + ": " + "BYE sip:" + addrClient + " SIP/2.0.")
                            data2 = my_socket.recv(2048)
                            if data2 == b"":
                                data2 = my_socket.recv(2048)
                                Answer3 = data2.decode('utf-8')
                                answer3 = Answer3.split("\r\n")[0]

                                if answer3 == "SIP/2.0 200 OK":
                                    TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                                    print(TIME + " " + "SIP" + " " + "from" + " " + SERVER + ":" + str(portReg) + answer3)
                                    serv.shutdown()
                                    # Cerramos el fichero donde estamos escribiendo los datos recibidos
                                RTPHandler.close_output()
                                print("Time passed, shutting down receiver loop.")
                                print("Terminando conexión...")
                    except ConnectionRefusedError:
                        print("Error al conectarse con servidor ")

                else:
                    print("ERROR")



if __name__ == "__main__":
    main()
