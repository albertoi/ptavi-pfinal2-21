#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import time
import json


PORT = int(sys.argv[1])
SERVER = "localhost"


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}

    def register2json(self):

        nameJson = "registered.json"
        with open(nameJson, 'w') as fichero:
            json.dump(self.dicc, fichero, indent=3)

    def json2registered(self):

        nameJson = "registered.json"
        try:
            with open(nameJson, 'r') as fichero:
                self.dicc = json.load(fichero)
        except FileNotFoundError:
            print("Error on registered.json file")

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        ok = "SIP/2.0 200 OK"
        line = self.rfile.read().decode("utf-8")
        client = line.split(' ')[1]
        expires = line.split("\r\n")[1].split(" ")[1]

        if line.split(' ')[0] == 'REGISTER':
            if len(self.dicc) >= 0:
                self.json2registered()
                ip = line.split("\r\n")[1].split(":")[2]
                port = line.split("\r\n")[1].split(":")[1].split(" ")[3]
                TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime())
                print(TIME + " " + "SIP" + " " + "from " + ":" + str(port))
                self.dicc[client] = [str(ip) + ":" + str(port)]
                self.register2json()
                self.wfile.write(ok.encode("utf-8"))
                if int(expires) == 0:
                    del self.dicc[client]
                elif int(expires) != 0:
                    TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime())
                    self.dicc[client] = ["dir: " + port, "expires: " + TIME]
                    lista = []
                    for list in self.dicc:
                        exp = " ".join(self.dicc[list][1].split(" ")[1:])

                        if exp <= TIME:
                            lista.append(list)

                    for usuario in lista:
                        del self.dicc[usuario]


                    print("SIP" + " " + "to" + " " + str(port))
                    self.wfile.write(ok.encode('utf-8'))

        else:
            print("Registrandose")


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer((SERVER, PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()  # el servidor va a estar siempre escuchando hasta que reciba una peticion,
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
