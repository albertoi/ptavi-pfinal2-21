#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import time
import json
import socket

PORT = int(sys.argv[1])
SERVER = "localhost"



class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}

    def register2json(self):

        nameJson = "registered.json"
        with open(nameJson, 'w') as fichero:
            json.dump(self.dicc, fichero, indent=3)

    def json2registered(self):

        nameJson = "registered.json"
        try:
            with open(nameJson, 'r') as fichero:
                self.dicc = json.load(fichero)
        except FileNotFoundError:
            print("Error on registered.json file")


    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        line = self.rfile.read()
        if line.decode("utf-8").split(' ')[0] == "INVITE":
            try:
                with open("registered.json", 'r') as fichero:
                    self.dicc = json.load(fichero)
                    client = line.decode("utf-8").split(" ")[1]
                    print(client)
                    if str(client) in self.dicc:

                       PUERTOserver = int(self.dicc[client][0].split(":")[1])
                       IPserver = str(self.dicc[client][0].split(":")[0])
                       TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                       print(TIME + " " + "SIP" + " " + "from " + IPserver + ":" + str(PUERTOserver))

                       try:
                            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

                                data1 = line
                                my_socket.connect((SERVER, PUERTOserver))
                                my_socket.sendto(data1, (SERVER, PUERTOserver))
                                data = my_socket.recv(2048)
                                Answer = data.decode("utf-8")
                                answer = Answer.split("\r\n")[0]
                                if answer == "SIP/2.0 200 OK":
                                    TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                                    print(TIME + " " + "SIP" + " " + "to " + IPserver + ":" + str(PUERTOserver))
                                    self.wfile.write(data)
                       except ConnectionRefusedError:
                            print("Error ")
            except FileNotFoundError:
                print("SIN REGISTRO")
        elif line.decode("utf-8").split(' ')[0] == 'ACK':
            try:
                with open("registered.json", 'r') as fichero:
                    self.dicc = json.load(fichero)
                    print(self.dicc)
                    client = line.decode("utf-8").split(' ')[1].split(':')[1]
                    if client in self.dicc:
                        IPserver = self.dicc[client][0].split(":")[0]
                        PUERTOserver = int(self.dicc[client][0].split(":")[1])
                        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                        print(TIME + " " + "SIP" + " " + "from " + IPserver + ":" + str(PUERTOserver))

                        try:
                            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                                my_socket.connect((SERVER, PUERTOserver))
                                d2 = line
                                my_socket.sendto(d2, (IPserver, PUERTOserver))

                        except ConnectionRefusedError:
                            print("Error ")
            except FileNotFoundError:
                print("SIN REGISTRO")
        elif line.decode("utf-8").split(' ')[0] == "BYE":
            try:
                with open("registrar.json", 'r') as fichero:
                    self.dicc = json.load(fichero)
                    client = line.decode("utf-8").split(" ")[1]

                    if str(client) in self.dicc:
                        PUERTOserver = int(self.dicc[client][0].split(":")[1])
                        IPserver = str(self.dicc[client][0].split(":")[0])
                        TIME = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
                        print(TIME + " " + "SIP" + " " + "from " + IPserver + ":" + str(PUERTOserver))

                        try:
                            with socket.socket(socket.AF_INET,
                                               socket.SOCK_DGRAM) as my_socket:
                                my_socket.connect((SERVER, PUERTOserver))
                                d = line
                                my_socket.sendto(d, (SERVER, PUERTOserver))
                                data = my_socket.recv(2048)

                                Answer2 = data.decode('utf-8')
                                answer2 = Answer2.split("\r\n")[0]

                                if answer2 == "SIP/2.0 200 OK":
                                    print(TIME + " " + "SIP" + " " + "sto " + IPserver + ":" + str(PUERTOserver))
                                    self.wfile.write(data)

                        except ConnectionRefusedError:
                            print("Error conectando a servidor")

            except FileNotFoundError:
                print("NO HAY REGISTRO")


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer((SERVER, PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()  # el servidor va a estar siempre escuchando hasta que reciba una peticion,
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
